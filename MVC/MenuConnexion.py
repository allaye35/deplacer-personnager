#import csv
import json

from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
import UI
import Personnage



class MenuConnexionVue(GridLayout):
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.controller = controller
        self.grid1 = GridLayout(cols=4)
        self.input_prenom = None
        self.add_widget(self.grid1)

        self.lastName = None
        self.firstName = None
        self.age = None
        self.comptes = {"personnages": []}
        self.charger()
        self.layout1()

    def layout1(self):

        self.grid1.add_widget(Label(text="last name:"))
        self.lastName = TextInput(multiline=False)
        self.grid1.add_widget(self.lastName)

        self.grid1.add_widget(Label(text="first name:"))
        self.firstName = TextInput(multiline=False)
        self.grid1.add_widget(self.firstName)

        self.grid1.add_widget(Label(text="age:"))
        self.age = TextInput(multiline=False)
        self.grid1.add_widget(self.age)

        valider = UI.Bouton(text="Crée_un_compte")
        valider.bind(on_press=self.sauvegader_jeu_identifiant_joueur)
                     #on_release=self.lecteur_De_Fichier_compte_Sauvegarder)
        self.grid1.add_widget(valider)


        b2 = UI.Bouton(text="se_Connecter")
        self.grid1.add_widget(b2)
        b2.bind(on_press=self.connecter)

    def sauvegader_jeu_identifiant_joueur(self, widget):
        self.position= {'center_x': 0.5, 'center_y': 0.5}
        liste_comptes=self.comptes["personnages"]
        print(" liste_comptes", liste_comptes)
        liste_comptes.append([self.lastName.text, self.firstName.text, self.age.text, self.position])
        self.comptes["personnages"]=liste_comptes
        print("self.comptes", self.comptes)
        print("self.lastName", type(self.firstName.text))

        with open("fichier.json", 'w') as fichier:
            json.dump(self.comptes, fichier)
            print('self.comptes', self.comptes)

    def charger(self):
        with open("fichier.json", 'r') as lecture_fichier:
            lecture_fichier_json = json.load(lecture_fichier)
            self.comptes=lecture_fichier_json
            print('self.comptes ahah', self.comptes)

    def connecter(self, widget):
        liste_compte_saisie=[self.lastName.text, self.firstName.text, self.age.text]
        liste_comptes = self.comptes["personnages"]

        for compte in liste_comptes:
            print("compte de connexion", compte)
            if liste_compte_saisie[0]==compte[0] and liste_compte_saisie[1]==compte[1] and liste_compte_saisie[2]==compte[2]:
                print("je suis connecte compte", compte)
                self.clbk_menu_images()
            else:
                print("vous avez pas de compter existant")
    #cette fonction permet d'affiche?
    def clbk_menu_images(self):
        self.controller.naviguer_menu_map(self.lastName.text, self.firstName.text, self.age.text)


class MenuConnexionController():
    def __init__(self, menus):
        self.menus = menus
        self.vue = MenuConnexionVue(self)
        self.personnages = []

    # cette fonction me renvoi sur la pages Menu Map,sur laquelle mon personnager et ces boutton se trouves
    def naviguer_menu_map(self, lastName, firstName, age):
        self.menus.afficher_menu(self.menus.menu_map)
        print("navaiguer menu map", lastName)
        print("navaiguer menu map", firstName)
        print("navaiguer menu map", age)
        self.menus.menu_map.recuperation_compte(lastName, firstName, age)
        self.menus.menu_map.reprise_de_jeu()


    def charger_utilisateurs(self):
        # ouvrir le fichier et créer les objets personnages
        pass

    def enregistrer_utilisateur(self, prenom):
        p = Personnage.Personnage(prenom)
        print("Utilisateur enregistré : ", prenom)
        self.personnages.append(p)
