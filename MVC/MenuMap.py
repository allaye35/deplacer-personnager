import csv
import json

from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput

import UI
from Personnage import Personnage


class MenuMapVue(GridLayout):
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.controller = controller
        #j'ajoute à mon gridlayout le nombre de colone et de ligne
        self.grid1 = GridLayout(cols=2, rows=1)
        self.add_widget(self.grid1)

        self.floatlayout= FloatLayout(size=(300, 300))
        self.grid1.add_widget(self.floatlayout)

        self.grid2 = GridLayout(cols=1, rows=6, size_hint=(0.05, 0.05))
        #self.grid2.orientation=None
        self.grid1.add_widget(self.grid2)
        self.image=None
        self.layout1()

    def layout1(self):

        #self.grid1.clear_widgets()
        b1 = UI.Label(text="menu_connexion")
        b1.bind(on_press=self.clbk_menu_connexion)
        imageBoutton = UI.Bouton(text="imageBoutton")
        self.image = Image(source="personnagerAvion.jpg", size_hint=(0.3, 0.3))
        self.floatlayout.add_widget(self.image)
        self.imageBoutton = UI.Bouton(text="imageBoutton")
        self.grid2.add_widget(self.imageBoutton)
        self.imageBoutton.bind(on_press=self.retour_centre_de_jeu)

        directionHaut = UI.Bouton(text="^")
        self.grid2.add_widget(directionHaut)
        directionHaut.bind(on_press=self.analoguer_direction_Haut)

        directionBas = UI.Bouton(text="v")
        self.grid2.add_widget(directionBas)
        directionBas.bind(on_press=self.analoguer_direction_Bas)

        directionDroite = UI.Bouton(text=">")
        self.grid2.add_widget(directionDroite)
        directionDroite.bind(on_press=self.analoguer_direction_Droite)

        directionGauche = UI.Bouton(text="<")
        self.grid2.add_widget(directionGauche)
        directionGauche.bind(on_press=self.analoguer_direction_Gauche)

        retour = UI.Bouton(text="retour")
        self.grid2.add_widget(retour)
        retour.bind(on_press=self.clbk_menu_connexion)
    #cette fonction permet de situer le personnager au centre de jeu
    def retour_centre_de_jeu(self, widget):
        self.controller.centre_de_jeu()

    # cette fonction permet de diriger  mon personnager en haut
    def analoguer_direction_Haut(self, widget):
        self.controller.new_position_Haut()
        self.image.pos_hint = self.controller.new_position_Haut()
        print("haut", self.image.pos_hint)

    # cette fonction permet de diriger  mon personnager vers le Bas
    def analoguer_direction_Bas(self, widget):
        self.controller.new_position_Bas()
        self.image.pos_hint =self.controller.new_position_Bas()
        print("bas", self.image.pos_hint)

    # cette fonction permet de diriger  mon personnager vers la droite
    def analoguer_direction_Droite(self, widget):
        self.controller.new_position_Droite()
        self.image.pos_hint = self.controller.new_position_Droite()
        print("droite", self.image.pos_hint)

    # cette fonction permet de diriger  mon personnager vers la gauche
    def analoguer_direction_Gauche(self, widget):
        self.controller.new_position__Gauche()
        self.image.pos_hint = self.controller.new_position__Gauche()
        print("gauche", self.image.pos_hint)

    """" cette fonction appelle ma fonction  permettant de faire l'affichage, et lui donne commme argument
    ma menu connexion, qui lui me permet de créer un compte ou de se connecter sur un compter existant"""
    def clbk_menu_connexion(self, widget):
        self.controller.naviguer_menu_connexion()


   """ def sauvegader_jeu(self, widget):
        self.controleur.sauvegader_jeu_identifiant_joueur()"""

class MenuMapController():
    def __init__(self, menus):
        self.menus = menus
        self.vue = MenuMapVue(self)
        self.lastName = None
        self.firstName = None
        self.age = None
        self.personnage = Personnage(self.lastName, self.firstName, self.age)
        self.position=None
        self.comptes = {"personnages": []}

    # cette fonction permet de récupéré l' identifiant du personnager
    def recuperation_compte(self, lastName, firstName, age):
        self.lastName= lastName
        self.firstName = firstName
        self.age = age

    """cette fonction sauvegarder l'identifiant du joueur et la position du joueur avant d'appélé ma fonction de
     retour au menu connexion"""
    def naviguer_menu_connexion(self):
        self.sauvegader_identifiant_et_position_joueur()
        self.menus.afficher_menu(self.menus.menu_connexion)

    #cette fonction sauvegarder l'identifiant du joueur et la position du joueur
    def sauvegader_identifiant_et_position_joueur(self):
        print("self.lastname fonction", self.lastName)
        print("self.firstname", self.firstName)

        with open("fichier.json", 'r') as lecture_fichier:
            lecture_fichier_json = json.load(lecture_fichier)
            liste_comptes = lecture_fichier_json["personnages"]


            for compte in liste_comptes:
                print("self.lastname", self.lastName)
                print("self.firstname", self.firstName)
                if self.lastName==compte[0] and self.firstName ==compte[1] and self.age == compte[2]:
                    compte[3]=self.personnage.position
                    print("position modifier compter 3",  compte[3])
                    print("position modifier",  lecture_fichier_json)
                    with open("fichier.json", 'w') as fichier:
                        json.dump(lecture_fichier_json, fichier)
                        print('self.comptes',  lecture_fichier_json)
                else:
                    print("retour au compter de sauvegarder")

    def reprise_de_jeu(self):
        with open("fichier.json", 'r') as lecture_fichier:
            lecture_fichier_json = json.load(lecture_fichier)
            liste_comptes = lecture_fichier_json["personnages"]
            print("retour au jeu")
            for compte in liste_comptes:
                if self.lastName == compte[0] and self.firstName == compte[1] and self.age == compte[2]:
                    print("0101")
                    self.personnage.position = compte[3]
                    print("position modifier compter 3", compte[3])
                    print("position modifier", lecture_fichier_json)
                    print("retour au jeu de sauvegarder")
                    with open("fichier.json", 'w') as fichier:
                        json.dump(lecture_fichier_json, fichier)
                        print('self.comptes', lecture_fichier_json)
                        print("retour au jeu")
                else:
                    print("retour au compter de sauvegarder")

    def centre_de_jeu(self):
        with open("fichier.json", 'r') as lecture_fichier:
            lecture_fichier_json = json.load(lecture_fichier)
            liste_comptes = lecture_fichier_json["personnages"]
            print("retour au centre de jeu")
            for compte in liste_comptes:
                if self.lastName == compte[0] and self.firstName == compte[1] and self.age == compte[2]:
                    print("0101")
                    compte[3]={'center_x': 0.5, 'center_y': 0.5}
                    #self.personnage.position = {'center_x': 0.5, 'center_y': 0.5}
                    print("position modifier compter 3", compte[3])
                    print("position modifier", lecture_fichier_json)
                    print("retour au centre de jeu")
                    with open("fichier.json", 'w') as fichier:
                        json.dump(lecture_fichier_json, fichier)
                        print('self.comptes', lecture_fichier_json)
                        print("retour au centre de jeu")
                else:
                    print("retour au compter de sauvegarder")

    def new_position_Haut(self):

        if self.personnage.position['center_y']<1:
            self.position_y = self.personnage.position['center_y'] + .1
            print(type(self.position_y))
            self.personnage.position= {'center_x': self.personnage.position['center_x'], 'center_y':self.position_y}
            print("haut", self.personnage.position)
            return self.personnage.position


        else:
            self.position_y = 0.0
            self.personnage.position= self.personnage.position= {'center_x': self.personnage.position['center_x'], 'center_y':self.position_y}
            print("haut", self.personnage.position)
            return self.personnage.position


    def new_position_Bas(self):

        if self.personnage.position['center_y'] > 0.0:
            self.position_y_bas= self.personnage.position['center_y'] - .1
            self.personnage.position = {'center_x':self.personnage.position['center_x'], 'center_y': self.position_y_bas}
            print("bas", self.personnage.position)
            return self.personnage.position


        else:

            self.position_y = 1
            self.personnage.position = self.personnage.position = {'center_x': self.personnage.position['center_x'],
                                                           'center_y': self.position_y}


            print("bas", self.personnage.position)
            return self.personnage.position

    def new_position_Droite(self):

        if self.personnage.position['center_x'] < 1:
            self.position_x = self.personnage.position['center_x'] + .1
            self.personnage.position = {'center_x': self.position_x, 'center_y': self.personnage.position['center_y']}
            print("Droite", self.personnage.position)
            return self.personnage.position

        else:
            self.position_x = 0.0
            self.personnage.position = self.personnage.position = {'center_x':  self.position_x,
                                                                   'center_y': self.personnage.position['center_y']}
            print("Droite", self.personnage.position)
            return self.personnage.position

    def new_position__Gauche(self):

        if self.personnage.position['center_x'] > 0.1:
            self.position_x_gauche = self.personnage.position['center_x'] - .1
            self.personnage.position = {'center_x':self.position_x_gauche, 'center_y': self.personnage.position['center_y']}
            print("Gauche", self.personnage.position)
            return self.personnage.position

        else:
            self.position_x = 1
            self.personnage.position = self.personnage.position = {'center_x': self.position_x,
                                                                   'center_y': self.personnage.position['center_y']}
            print("Gauche", self.personnage.position)
            return self.personnage.position
